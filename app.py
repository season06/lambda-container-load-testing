from __future__ import print_function
import json
import sys
import os
import logging

from os import getenv
from boto3 import client
from botocore.exceptions import ClientError

# REQUIRED: The table name to copy records to.
S3_BUCKET = getenv('S3_BUCKET')

# @see https://docs.python.org/3/library/logging.html#logging-levels
# LOGGING_LEVEL = getenv('LOGGING_LEVEL', 'INFO')

# # Set up logging level, and stream stream to stdout if not running in Lambda.
# # (We don't want to change the basicConfig if we ARE running in Lambda).
# logger = logging.getLogger(__name__)
# if getenv('AWS_EXECUTION_ENV') is None:
#   logging.basicConfig(stream=sys.stdout, level=LOGGING_LEVEL)
# else:
#   logger.setLevel(LOGGING_LEVEL)

# # Helper class to convert a DynamoDB item to JSON.
# class DecimalEncoder(json.JSONEncoder):
#     def default(self, o):
#         if isinstance(o, decimal.Decimal):
#             if o % 1 > 0:
#                 return float(o)
#             else:
#                 return int(o)
#         return super(DecimalEncoder, self).default(o)

# dynamodb = client('dynamodb', region_name='us-west-1')

def handler(event, context):
    os.system('cp -r /var/task/. /tmp')
    os.system(f'sh /tmp/load.sh {S3_BUCKET}')
    # logger.info('invoke event from jmeter %s' % json.dumps(event, indent=4, cls=DecimalEncoder))

    # upload test results to DynamoDB
    # Omit here…
    return {
        "statusCode": 200,
        "body": json.dumps({
            "HTTPStatusCode": 200,
        }),
    }
