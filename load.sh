#!/bin/bash
S3_BUCKET=${1}
echo "S3_BUCKET:: $S3_BUCKET"

echo "Running test"
bzt config.yml -o modules.console.disable=true | tee -a result.tmp
if [ -f /tmp/bzt-artifacts/results.xml ]
then
  echo "Validating Test Duration"
  TEST_DURATION=`xmlstarlet sel -t -v "/FinalStatus/TestDuration" /tmp/bzt-artifacts/results.xml`
  if (( $(echo "$TEST_DURATION > $CALCULATED_DURATION" | bc -l) ))
  then
    echo "Updating test duration: $CALCULATED_DURATION s"
    xmlstarlet ed -L -u /FinalStatus/TestDuration -v $CALCULATED_DURATION /tmp/bzt-artifacts/results.xml
  fi
  echo "Uploading results"
  aws s3 cp /tmp/bzt-artifacts/results.xml $S3_BUCKET/results.xml
else
  echo "Error happened while the test."
fi